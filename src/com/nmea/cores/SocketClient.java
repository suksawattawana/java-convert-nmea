package com.nmea.cores;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import org.apache.commons.codec.binary.Hex;

import com.nmea.gui.ClientUI;

public class SocketClient implements Runnable {

	private Thread thread = null;
	private Socket socket = null;
	private PrintWriter output;
	private BufferedReader input;
	private ClientUI ui;

	public SocketClient(ClientUI ui) {
		super();
		this.ui = ui;
	}

	public void connectServer(String arr, int port) {
		try {
			socket = new Socket(arr, port);
			output = new PrintWriter(socket.getOutputStream(), true);
			input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			thread = new Thread(this);
			thread.start();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/*
	 * Send message
	 */
	public void sendMessage(String msg) {
		output.println(Hex.encodeHexString(msg.getBytes()));
//		output.println(msg);
	}

	/*
	 * Receive message
	 */
	@Override
	public void run() {
		while (true) {
			try {
				String msg = input.readLine();
				getUi().getAreaDataReceive().append(msg);
			} catch (IOException e) {
				stop();
			}
		}
	}

	public void stop() {
		try {
			input.close();
			output.close();
			socket.close();
		} catch (IOException e) {}
		
	}

	// Getter and Setter
	// ---------------------------------------------------------------------------------------------

	public ClientUI getUi() {
		return ui;
	}

}