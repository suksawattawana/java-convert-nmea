package com.nmea.cores;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

import org.apache.commons.codec.DecoderException;

import com.enums.Direction;
import com.enums.Indicator;
import com.nmea.gui.ServerUI;
import com.util.Utils;
import com.util.bytes.ConvertBitByte;

public class SocketServer implements Runnable {

	private ServerSocket serverSocket = null;
	private Socket socket;
	private Thread thread = null;
	private PrintWriter output;
	private BufferedReader input;
	private ServerUI ui;

	public SocketServer(ServerUI ui) {
		super();
		this.ui = ui;
	}

	public void startServerSocket(int port) {
		try {
			serverSocket = new ServerSocket(port);
			System.out.println("Server started");
			socket = serverSocket.accept();
			output = new PrintWriter(socket.getOutputStream(), true);
			input = new BufferedReader(new InputStreamReader(socket.getInputStream()));

			thread = new Thread(this);
			thread.start();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/*
	 * Send message
	 */
	public void sendMessage() {
		String data = "";
		data += "$GPGGA";
		data += "," + Utils.reverseTimeZone(getUi().getFieldTimeZoneS().getText());
		data += "," + Utils.reverseLatitude(getUi().getFieldLatitudeS().getText());
		data += "," + getUi().getComboDirectionLatitudeS().getSelectedItem();
		data += "," + Utils.reverseLongitude(getUi().getFieldLongitudeS().getText());
		data += "," + getUi().getComboDirectionLongitudeS().getSelectedItem();
		data += "," + getUi().getComboGPSS().getSelectedIndex();
		data += "," + getUi().getFieldSVsS().getText();

		output.println(data);
	}

	/*
	 * Receive message
	 */
	@Override
	public void run() {
		while (true) {
			try {
				String msg = input.readLine();
				char[] msgHex = msg.toCharArray();
				String msgDec = ConvertBitByte.decodeHex(msgHex);
				convertNMEA(msgDec);

				System.out.println("Message from client: " + msg);
				System.out.println("Message from Dec: " + msgDec);
//				convertNMEA(input.readLine());
			} catch (IOException | DecoderException e) {
				stop();
			}
		}
	}

	public void stop() {
		try {
			input.close();
			output.close();
			socket.close();
		} catch (IOException e) {
		}

	}

	/*
	 * manage data of each section
	 */
	private void convertNMEA(String msg) {
		String[] lstData = msg.split(",");

		String data;
		for (int i = 0; i < lstData.length; i++) {
			data = lstData[i];
			switch (i) {
			case 0: {
				getUi().getFieldID().setText(data);
				break;
			}
			case 1: {
				getUi().getFieldTimeZone().setText(Utils.convertTimeZone(data));
				break;
			}
			case 2: {
				getUi().getFieldLatitude().setText(Utils.convertLatitude(data));
				break;
			}
			case 3: {
				String value = getUi().getFieldLatitude().getText() + Direction.findValue(data);
				getUi().getFieldLatitude().setText(value);
				break;
			}
			case 4: {
				getUi().getFieldLongitude().setText(Utils.convertLongitude(data));
				break;
			}
			case 5: {
				String value = getUi().getFieldLongitude().getText() + Direction.findValue(data);
				getUi().getFieldLongitude().setText(value);
				break;
			}
			case 6: {
				getUi().getFieldGPS().setText(Indicator.findValue(data));
				break;
			}
			case 7: {
				getUi().getFieldSVs().setText(data);
				break;
			}
			default:
				break;
			}

			if (i >= 8) {
				break;
			}
		}
	}

	// Getter and Setter
	// ---------------------------------------------------------------------------------------------

	public ServerUI getUi() {
		return ui;
	}

	public void setUi(ServerUI ui) {
		this.ui = ui;
	}

}
