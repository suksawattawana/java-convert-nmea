package com.nmea.gui;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFormattedTextField;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.enums.Direction;
import com.enums.Indicator;
import com.nmea.cores.SocketServer;
import com.util.RowHandler;
import com.util.Utils;
import com.util.component.ButtonStyle;
import com.util.component.ComboBoxStyle;
import com.util.component.JFrameStyle;
import com.util.component.LabelStyle;
import com.util.component.TextFieldStyle;

import net.miginfocom.swing.MigLayout;

public class ServerUI extends JFrameStyle {

	private static final long serialVersionUID = -355065526171762717L;
	private JPanel contentPane;
	private JPanel panelSend;
	private JPanel panelReceive;

	// Send
	private LabelStyle txtSend;
	private LabelStyle txtIDS;
	private LabelStyle txtTimeZoneS;
	private LabelStyle txtLatitudeS;
	private LabelStyle txtLongitudeS;
	private LabelStyle txtGPSS;
	private LabelStyle txtSVsS;
	private TextFieldStyle fieldIDS;
	private JFormattedTextField fieldTimeZoneS;
	private TextFieldStyle fieldLatitudeS;
	private ComboBoxStyle comboDirectionLatitudeS;
	private TextFieldStyle fieldLongitudeS;
	private ComboBoxStyle comboDirectionLongitudeS;
	private ComboBoxStyle comboGPSS;
	private TextFieldStyle fieldSVsS;
	private ButtonStyle btnSend;

	// Receive
	private LabelStyle txtReceive;
	private LabelStyle txtID;
	private LabelStyle txtTimeZone;
	private LabelStyle txtLatitude;
	private LabelStyle txtLongitude;
	private LabelStyle txtGPS;
	private LabelStyle txtSVs;
	private TextFieldStyle fieldID;
	private TextFieldStyle fieldTimeZone;
	private TextFieldStyle fieldLatitude;
	private TextFieldStyle fieldDirectionLatitude;
	private TextFieldStyle fieldLongitude;
	private TextFieldStyle fieldDirectionLongitude;
	private TextFieldStyle fieldGPS;
	private TextFieldStyle fieldSVs;

	private RowHandler rowContentPane = new RowHandler();
	private RowHandler rowPanelSend = new RowHandler();
	private RowHandler rowPanelReceive = new RowHandler();
	private SocketServer socketServer;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					new ServerUI().setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public ServerUI() {
		initComponent();
		socketServer = new SocketServer(this);
		getSocketServer().startServerSocket(1510);
	}

	private void initComponent() {
		customComponent();
		actionComponent();
		manageLayout();
		setContentPane(contentPane);
	}

	private void manageLayout() {
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new MigLayout("", "[grow]", "[][grow][][grow]"));
		contentPane.setBackground(Color.gray);
		contentPane.add(txtSend, rowContentPane.manageRow(0, "alignx center"));
		contentPane.add(panelSend, rowContentPane.manageRow(0, "grow", true));
		contentPane.add(txtReceive, rowContentPane.manageRow(0, "alignx center", true));
		contentPane.add(panelReceive, rowContentPane.manageRow(0, "grow", true));

		// Send
		panelSend.setLayout(new MigLayout("", "[][grow]", "[][][][][][][]"));
		panelSend.add(txtIDS, rowPanelSend.manageRow(0));
		panelSend.add(fieldIDS, rowPanelSend.manageRow(1, "growx"));
		panelSend.add(txtTimeZoneS, rowPanelSend.manageRow(0, true));
		panelSend.add(fieldTimeZoneS, rowPanelSend.manageRow(1, "growx"));
		panelSend.add(txtLatitudeS, rowPanelSend.manageRow(0, true));
		panelSend.add(fieldLatitudeS, rowPanelSend.manageRow(1, "growx"));
		panelSend.add(comboDirectionLatitudeS, rowPanelSend.manageRow(1));
		panelSend.add(txtLongitudeS, rowPanelSend.manageRow(0, true));
		panelSend.add(fieldLongitudeS, rowPanelSend.manageRow(1, "growx"));
		panelSend.add(comboDirectionLongitudeS, rowPanelSend.manageRow(1));
		panelSend.add(txtGPSS, rowPanelSend.manageRow(0, true));
		panelSend.add(comboGPSS, rowPanelSend.manageRow(1, "growx"));
		panelSend.add(txtSVsS, rowPanelSend.manageRow(0, true));
		panelSend.add(fieldSVsS, rowPanelSend.manageRow(1, "growx"));
		panelSend.add(btnSend, rowPanelSend.manageRow(1, "alignx center", true));

		// Receive
		panelReceive.setLayout(new MigLayout("", "[][grow]", "[][][][][][][][]"));
		panelReceive.add(txtID, rowPanelReceive.manageRow(0));
		panelReceive.add(fieldID, rowPanelReceive.manageRow(1, "growx"));
		panelReceive.add(txtTimeZone, rowPanelReceive.manageRow(0, true));
		panelReceive.add(fieldTimeZone, rowPanelReceive.manageRow(1, "growx"));
		panelReceive.add(txtLatitude, rowPanelReceive.manageRow(0, true));
		panelReceive.add(fieldLatitude, rowPanelReceive.manageRow(1, "growx"));
		panelReceive.add(txtLongitude, rowPanelReceive.manageRow(0, true));
		panelReceive.add(fieldLongitude, rowPanelReceive.manageRow(1, "growx"));
		panelReceive.add(txtGPS, rowPanelReceive.manageRow(0, true));
		panelReceive.add(fieldGPS, rowPanelReceive.manageRow(1, "growx"));
		panelReceive.add(txtSVs, rowPanelReceive.manageRow(0, true));
		panelReceive.add(fieldSVs, rowPanelReceive.manageRow(1, "growx"));

	}

	private void actionComponent() {
		btnSend.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				getSocketServer().sendMessage();
			}
		});
	}

	private void customComponent() {
		contentPane = new JPanel();
		panelSend = new JPanel();
		panelReceive = new JPanel();

		// Send
		txtSend = new LabelStyle("Send");
		txtIDS = new LabelStyle("MsgID");
		txtTimeZoneS = new LabelStyle("TimeZone");
		txtLatitudeS = new LabelStyle("Latitude");
		txtLongitudeS = new LabelStyle("Longitude");
		txtGPSS = new LabelStyle("GPS");
		txtSVsS = new LabelStyle("Number of SVs");
		fieldIDS = new TextFieldStyle();
		fieldTimeZoneS = Utils.formatTimeZone();
		fieldLatitudeS = new TextFieldStyle();
		comboDirectionLatitudeS = new ComboBoxStyle(Direction.findKeyByLatitude());
		fieldLongitudeS = new TextFieldStyle();
		comboDirectionLongitudeS = new ComboBoxStyle(Direction.findKeyByLongitude());
		comboGPSS = new ComboBoxStyle(Indicator.listValue());
		fieldSVsS = new TextFieldStyle();
		btnSend = new ButtonStyle("Send");
		

		// Receive
		txtReceive = new LabelStyle("Receive");
		txtID = new LabelStyle("MsgID");
		txtTimeZone = new LabelStyle("TimeZone");
		txtLatitude = new LabelStyle("Latitude");
		txtLongitude = new LabelStyle("Longitude");
		txtGPS = new LabelStyle("GPS");
		txtSVs = new LabelStyle("Number of SVs");
		fieldID = new TextFieldStyle();
		fieldTimeZone = new TextFieldStyle();
		fieldLatitude = new TextFieldStyle();
		fieldDirectionLatitude = new TextFieldStyle();
		fieldLongitude = new TextFieldStyle();
		fieldDirectionLongitude = new TextFieldStyle();
		fieldGPS = new TextFieldStyle();
		fieldSVs = new TextFieldStyle();
	}

	// Getter and Setter
	// ---------------------------------------------------------------------------------------------

	public SocketServer getSocketServer() {
		return socketServer;
	}

	public TextFieldStyle getFieldID() {
		return fieldID;
	}

	public void setFieldID(TextFieldStyle fieldID) {
		this.fieldID = fieldID;
	}

	public TextFieldStyle getFieldTimeZone() {
		return fieldTimeZone;
	}

	public void setFieldTimeZone(TextFieldStyle fieldTimeZone) {
		this.fieldTimeZone = fieldTimeZone;
	}

	public TextFieldStyle getFieldLatitude() {
		return fieldLatitude;
	}

	public void setFieldLatitude(TextFieldStyle fieldLatitude) {
		this.fieldLatitude = fieldLatitude;
	}

	public TextFieldStyle getFieldDirectionLatitude() {
		return fieldDirectionLatitude;
	}

	public void setFieldDirectionLatitude(TextFieldStyle fieldDirectionLatitude) {
		this.fieldDirectionLatitude = fieldDirectionLatitude;
	}

	public TextFieldStyle getFieldLongitude() {
		return fieldLongitude;
	}

	public void setFieldLongitude(TextFieldStyle fieldLongitude) {
		this.fieldLongitude = fieldLongitude;
	}

	public TextFieldStyle getFieldDirectionLongitude() {
		return fieldDirectionLongitude;
	}

	public void setFieldDirectionLongitude(TextFieldStyle fieldDirectionLongitude) {
		this.fieldDirectionLongitude = fieldDirectionLongitude;
	}

	public TextFieldStyle getFieldGPS() {
		return fieldGPS;
	}

	public void setFieldGPS(TextFieldStyle fieldGPS) {
		this.fieldGPS = fieldGPS;
	}

	public TextFieldStyle getFieldSVs() {
		return fieldSVs;
	}

	public void setFieldSVs(TextFieldStyle fieldSVs) {
		this.fieldSVs = fieldSVs;
	}

	public TextFieldStyle getFieldIDS() {
		return fieldIDS;
	}

	public void setFieldIDS(TextFieldStyle fieldIDS) {
		this.fieldIDS = fieldIDS;
	}

	public JFormattedTextField getFieldTimeZoneS() {
		return fieldTimeZoneS;
	}

	public void setFieldTimeZoneS(JFormattedTextField fieldTimeZoneS) {
		this.fieldTimeZoneS = fieldTimeZoneS;
	}

	public TextFieldStyle getFieldLatitudeS() {
		return fieldLatitudeS;
	}

	public void setFieldLatitudeS(TextFieldStyle fieldLatitudeS) {
		this.fieldLatitudeS = fieldLatitudeS;
	}

	public ComboBoxStyle getComboDirectionLatitudeS() {
		return comboDirectionLatitudeS;
	}

	public void setComboDirectionLatitudeS(ComboBoxStyle comboDirectionLatitudeS) {
		this.comboDirectionLatitudeS = comboDirectionLatitudeS;
	}

	public TextFieldStyle getFieldLongitudeS() {
		return fieldLongitudeS;
	}

	public void setFieldLongitudeS(TextFieldStyle fieldLongitudeS) {
		this.fieldLongitudeS = fieldLongitudeS;
	}

	public ComboBoxStyle getComboDirectionLongitudeS() {
		return comboDirectionLongitudeS;
	}

	public void setComboDirectionLongitudeS(ComboBoxStyle comboDirectionLongitudeS) {
		this.comboDirectionLongitudeS = comboDirectionLongitudeS;
	}

	public ComboBoxStyle getComboGPSS() {
		return comboGPSS;
	}

	public void setComboGPSS(ComboBoxStyle comboGPSS) {
		this.comboGPSS = comboGPSS;
	}

	public TextFieldStyle getFieldSVsS() {
		return fieldSVsS;
	}

	public void setFieldSVsS(TextFieldStyle fieldSVsS) {
		this.fieldSVsS = fieldSVsS;
	}

}