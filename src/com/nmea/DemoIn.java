package com.nmea;

public class DemoIn {

	public static void main(String[] args) {

		String data = "$GPGGA,172814.0,3723.46587704,N,12202.26957864,W,2,6,1.2,18.893,M,-25.669,M,2.0,0031*4F";

		String time = convertLongitude("12202.26957864");
		System.out.println(time);
	}

	public static String convertLongitude(String data) {
		String result = null;

		String[] splitData = data.replace(".", ",").split(",");
		String value = splitData[0];
		
		if (value.startsWith("0") == true) {
			value = value.replaceFirst("0", "");
		}

		int length = value.length();
		if (length >= 4) {
			int poi = (length == 5) ? 3 : 2 ;
			result = value.substring(0, 2) + " deg " + value.substring(length - poi, length);
		}

		if (splitData.length > 1) {
			result += "." + value + "'";
		}

		return result;
	}
}
