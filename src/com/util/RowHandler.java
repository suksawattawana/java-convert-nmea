package com.util;

public class RowHandler {

	private short rowNum = 0;

	public RowHandler() {
		rowNum = 0;
	}

	/**
	 * @param colStart - column �������
	 * @return String
	 */
	public String manageRow(int colStart) {
		return manageRowAndCol(colStart, 0, 0, null, false);
	}

	/**
	 * @param colStart - column �������
	 * @param newRow   - ��� row ����
	 * @return String
	 */
	public String manageRow(int colStart, boolean newRow) {
		return manageRowAndCol(colStart, 0, 0, null, newRow);
	}

	/**
	 * @param colStart   - column �������
	 * @param constrains - constrains - ����Ѻ���س���ѵԡ�èѴ���˹�
	 * @return String
	 */
	public String manageRow(int colStart, String constrains) {
		return manageRowAndCol(colStart, 0, 0, constrains, false);
	}

	/**
	 * @param int     colStart
	 * @param boolean newRow
	 * @return String
	 */
	public String manageRow(int colStart, String constrains, boolean newRow) {
		return manageRowAndCol(colStart, 0, 0, constrains, newRow);
	}

	/**
	 * @param int     colStart
	 * @param boolean newRow
	 * @return String
	 */
	public String manageRow(int colStart, int colEnd, int rowSize, String constrains, boolean newRow) {
		return manageRowAndCol(colStart, colEnd, rowSize, constrains, newRow);
	}

	/*
	 * main
	 */
	private String manageRowAndCol(int colStart, int colEnd, int rowSize, String constrains, boolean newRow) {
		String result = "cell", spec = " ";

		result += spec + colStart;
		result += spec + ((newRow) ? ++rowNum : rowNum);

		if (colEnd > 0 || rowSize > 0) {
			result += spec + colEnd;
			result += spec + rowSize;
		}

		if (constrains != null && !constrains.isEmpty()) {
			result += spec + ", " + constrains;
		}

		return result;
	}
}
