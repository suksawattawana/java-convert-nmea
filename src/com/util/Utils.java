package com.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JFormattedTextField;
import javax.swing.text.MaskFormatter;

public class Utils {

	// check port
	public static int checkAndParsePort(String port) {
		int result = Integer.parseInt(port);
		return result;
	}

	public static JFormattedTextField formatTimeZone() {
		try {
			MaskFormatter mask = new MaskFormatter("##:##:##.##");
			mask.setPlaceholderCharacter('_');
			return new JFormattedTextField(mask);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Message to GGA
	 * 
	 * @param time
	 * @return
	 */
	public static double reverseTimeZone(String time) {
		String result = null;
		Date date = new Date();
		SimpleDateFormat before = new SimpleDateFormat("HH:mm:ss.SS");
		SimpleDateFormat after = new SimpleDateFormat("HHmmss.SS");

		try {
			if (time != null && !time.replaceAll("[^0-9]", "").isEmpty()) {
				date = before.parse(time);
				result = after.format(date);
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return Double.parseDouble(result);
	}

	/**
	 * GGA to Message
	 * 
	 * @param data
	 * @return
	 */
	public static String convertTimeZone(String data) {
		String result = null;
		String hh, mm, ss;

		String[] splitData = data.replace(".", ",").split(",");
		String value = splitData[0];
		int length = value.length();

		// hh:mm:ss
		if (length <= 2) {
			hh = "00";
			mm = "00";
			ss = value.substring(0, length - 2);
			result = hh + ":" + mm + ":" + ss;
		} else if (length <= 4) {
			hh = "00";
			mm = value.substring(0, length - 2);
			ss = value.substring(length - 2, length);
			result = hh + ":" + mm + ":" + ss;
		} else if (length <= 6) {
			hh = value.substring(0, length - 4);
			mm = value.substring(length - 4, length - 2);
			ss = value.substring(length - 2, length);
			result = hh + ":" + mm + ":" + ss;
		}

		// decimal
		if (splitData.length > 1) {
			value = splitData[1];
			result += "." + value;
		}

		return result;
	}

	/**
	 * GGA to Message
	 * 
	 * @param data
	 * @return
	 */
	public static String convertLatitude(String data, String direction) {
		String result = null;

		String[] splitData = data.replace(".", ",").split(",");
		String value = splitData[0];
		int length = value.length();

		if (length >= 4) {
			result = value.substring(0, length - 2) + " deg " + value.substring(length - 2, length);
		}

		if (splitData.length > 1) {
			result += "." + value + "' ";
		}

		return result + direction;
	}
	
	public static String convertLatitude(String data) {
		String result = null;

		String[] splitData = data.replace(".", ",").split(",");
		String value = splitData[0];
		int length = value.length();

		if (length >= 4) {
			result = value.substring(0, length - 2) + " deg " + value.substring(length - 2, length);
		}

		if (splitData.length > 1) {
			result += "." + value + "' ";
		}

		return result;
	}

	/**
	 * Message to GGA
	 * 
	 * @param time
	 * @return
	 */
	public static double reverseLatitude(String data) {
		return (data != null) ? Double.parseDouble(data.replaceAll("[^0-9.]", "")) : 0;
	}

	/**
	 * GGA to Message
	 * 
	 * @param data
	 * @return
	 */
	public static String convertLongitude(String data, String direction) {
		String result = null;

		String[] splitData = data.replace(".", ",").split(",");
		String value = splitData[0];

		if (value.startsWith("0") == true) {
			value = value.replaceFirst("0", "");
		}

		int length = value.length();
		if (length >= 4) {
			int poi = (length == 5) ? 3 : 2;
			result = value.substring(0, 2) + " deg " + value.substring(length - poi, length);
		}

		if (splitData.length > 1) {
			result += "." + value + "' ";
		}

		return result + direction;
	}
	
	public static String convertLongitude(String data) {
		String result = null;

		String[] splitData = data.replace(".", ",").split(",");
		String value = splitData[0];

		if (value.startsWith("0") == true) {
			value = value.replaceFirst("0", "");
		}

		int length = value.length();
		if (length >= 4) {
			int poi = (length == 5) ? 3 : 2;
			result = value.substring(0, 2) + " deg " + value.substring(length - poi, length);
		}

		if (splitData.length > 1) {
			result += "." + value + "' ";
		}

		return result;
	}

	public static double reverseLongitude(String data) {
		return (data != null) ? Double.parseDouble(data.replaceAll("[^0-9.]", "")) : 0;
	}
}
