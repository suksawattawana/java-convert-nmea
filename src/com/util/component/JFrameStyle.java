package com.util.component;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.HeadlessException;
import java.awt.Image;
import java.awt.Toolkit;
import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

public class JFrameStyle extends JFrame {

	private static final long serialVersionUID = -6094678923661189904L;
	private int x_window, y_window, size_width = 800, size_height = 600;
	private ResourceBundle bundle;
	private final static String CORDIA_NEW = "Cordia New";

	public JFrameStyle() throws HeadlessException {
		bundle = ResourceBundle.getBundle("resources/properties/messages", Locale.UK);
		findScreenSizeWindowForProtionStart();
		super.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		super.setBounds(x_window, y_window, size_width, size_height);
		super.setIconImage(checkAndGetImage(bundle.getString("imgTitle")));
		super.setTitle(bundle.getString("title"));
	}

	/*
	 * manage position window screen when start application, position form center
	 */
	private void findScreenSizeWindowForProtionStart() {
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		x_window = (int) (screenSize.getWidth() / 2) - (size_width / 2);
		y_window = (int) (screenSize.getHeight() / 2) - (size_height / 2);
	}

	/*
	 * check image path and get image
	 */
	private Image checkAndGetImage(String path) {
		try {
			if (path != null && !path.isEmpty()) {
				URL classPath = getClass().getResource(path);
				return Toolkit.getDefaultToolkit().getImage(classPath);
			}
		} catch (Exception e) {
		}
		return null;
	}
}
