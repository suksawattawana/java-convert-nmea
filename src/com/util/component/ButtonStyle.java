package com.util.component;

import javax.swing.JButton;

import com.util.FontStyle;

public class ButtonStyle extends JButton {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ButtonStyle() {
		super();
	}

	public ButtonStyle(String text) {
		super();
		super.setFont(FontStyle.cordiaNew());
		super.setText(text);
	}
	
	

}
