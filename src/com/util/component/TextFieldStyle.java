package com.util.component;

import javax.swing.JTextField;

import com.util.FontStyle;

public class TextFieldStyle extends JTextField {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TextFieldStyle() {
		super.setFont(FontStyle.cordiaNew());
	}
	
}
