package com.util.component;

import javax.swing.JLabel;

import com.util.FontStyle;

public class LabelStyle extends JLabel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public LabelStyle() {
		super();
	}

	public LabelStyle(String text) {
		super.setFont(FontStyle.cordiaNew());
		super.setText(text);
	}

}
