package com.util.component;

import javax.swing.ComboBoxModel;
import javax.swing.JComboBox;

@SuppressWarnings({ "unchecked", "rawtypes" })
public class ComboBoxStyle extends JComboBox {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6426157293116446123L;

	public ComboBoxStyle() {
		super();
	}

	public ComboBoxStyle(ComboBoxModel<?> aModel) {
		super(aModel);
	}

	public ComboBoxStyle(Object[] items) {
		super(items);
	}

}
