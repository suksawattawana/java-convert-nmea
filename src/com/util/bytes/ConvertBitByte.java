package com.util.bytes;

import java.math.BigInteger;

import org.apache.commons.codec.DecoderException;

public class ConvertBitByte {

	/*
	 * BIN
	 * ---------------------------------------------------------------------------
	 */
	public static int BINtoDEC(long bin) {
		return Integer.parseInt(String.valueOf(bin), 2);
	}

	public static int BINtoOCT(long bin) {
		return Integer.parseInt(new BigInteger(String.valueOf(bin), 2).toString(8));
	}

	public static String BINtoHEX(long bin) {
		return new BigInteger(String.valueOf(bin), 2).toString(16);
	}

	/*
	 * OCT
	 * ---------------------------------------------------------------------------
	 */
	public static long OCTtoBIN(int oct) {
		return Integer.parseInt(new BigInteger(String.valueOf(oct), 8).toString(2));
	}

	public static int OCTtoDEC(int oct) {
		return Integer.parseInt(new BigInteger(String.valueOf(oct), 8).toString(10));
	}

	public static String OCTtoHEX(int oct) {
		return new BigInteger(String.valueOf(oct), 8).toString(16);
	}

	/*
	 * DEC
	 * ---------------------------------------------------------------------------
	 */
	public static long DECtoBIN(int dec) {
		return Long.parseLong(new BigInteger(String.valueOf(dec), 10).toString(2));
	}

	public static int DECtoOCT(int dec) {
		return Integer.parseInt(new BigInteger(String.valueOf(dec), 10).toString(8));
	}

	public static String DECtoHEX(int dec) {
		return new BigInteger(String.valueOf(dec), 10).toString(16);
	}

	public static String DECtoHEX(String dec) {
		return new BigInteger(dec, 10).toString(16);
	}

	/*
	 * HEX
	 * ---------------------------------------------------------------------------
	 */
	public static long HEXtoBIN(String hex) {
		return Long.parseLong(new BigInteger(hex, 16).toString(2));
	}

	public static int HEXtoOCT(String hex) {
		return Integer.parseInt(new BigInteger(hex, 16).toString(8));
	}

	public static int HEXtoDEC(String hex) {
		return Integer.parseInt(hex, 16);
	}

	public static String decodeHex(final char[] data) throws DecoderException {
		String result = "";
		final int lengthHex = data.length;
		int dec = 0;
		
		// two characters form the hex value.
        for (int j = 0; j < lengthHex; j++) {
            dec = Character.digit(data[j], 16) << 4;
            dec = dec | Character.digit(data[++j], 16);
            result +=  (char) (dec & 0xFF);
        }
        return result;
	}

}
