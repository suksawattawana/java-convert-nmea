package com.nmea_object.cores.model;

import java.io.Serializable;

public class GPGGA implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4158011252948331126L;
	private String id;
	private double time;
	private double latitude;
	private String directionLatitude;
	private double longitude;
	private String directionLongitude;
	private int gpsStatus;
	private int numberOfSats;

	@Override
	public String toString() {
		return "GPGGA [id=" + id + ", time=" + time + ", latitude=" + latitude + ", directionLatitude="
				+ directionLatitude + ", longitude=" + longitude + ", directionLongitude=" + directionLongitude
				+ ", gpsStatus=" + gpsStatus + ", numberOfSats=" + numberOfSats + "]";
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public double getTime() {
		return time;
	}

	public void setTime(double time) {
		this.time = time;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public String getDirectionLatitude() {
		return directionLatitude;
	}

	public void setDirectionLatitude(String directionLatitude) {
		this.directionLatitude = directionLatitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public String getDirectionLongitude() {
		return directionLongitude;
	}

	public void setDirectionLongitude(String directionLongitude) {
		this.directionLongitude = directionLongitude;
	}

	public int getGpsStatus() {
		return gpsStatus;
	}

	public void setGpsStatus(int gpsStatus) {
		this.gpsStatus = gpsStatus;
	}

	public int getNumberOfSats() {
		return numberOfSats;
	}

	public void setNumberOfSats(int numberOfSats) {
		this.numberOfSats = numberOfSats;
	}

}
