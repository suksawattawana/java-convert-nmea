package com.nmea_object.cores.model;

public class CommonMsg {
	private String header;
	private GPGGA gga;

	public String getHeader() {
		return header;
	}
	public void setHeader(String header) {
		this.header = header;
	}
	public GPGGA getGga() {
		return gga;
	}
	public void setGga(GPGGA gga) {
		this.gga = gga;
	}
	
	
	
}
