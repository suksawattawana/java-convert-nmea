package com.nmea_object.cores;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import com.nmea_object.cores.model.GPGGA;
import com.nmea_object.gui.ClientUI;

public class SocketClient implements Runnable {

	private Thread thread = null;
	private Socket socket = null;
	private ObjectInputStream input;
	private ObjectOutputStream output;
	private ClientUI ui;
	private GPGGA gpGGA;

	public SocketClient(ClientUI ui) {
		super();
		this.ui = ui;
	}

	public void connectServer(String arr, int port) {
		try {
			socket = new Socket(arr, port);
			System.out.println("Client connected to server: " + socket.getLocalPort());
			
			output = new ObjectOutputStream(socket.getOutputStream());
			output.flush();
			input = new ObjectInputStream(socket.getInputStream());
			
			thread = new Thread(this);
			thread.start();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/*
	 * Send message
	 */
	public void sendMessage(String msg) {
		try {
			output.writeObject(convertNMEA(msg));
			output.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/*
	 * Receive message
	 */
	@Override
	public void run() {
		while (true) {
			try {
				setGpGGA((GPGGA) input.readObject());
				getUi().getAreaDataReceive().append(getGpGGA().toString());
			} catch (ClassNotFoundException e) {
				stop(e.getMessage());
			} catch (IOException e) {
				stop(e.getMessage());
			}
		}
	}

	public void stop(String errorMsg) {
		System.out.println(errorMsg);
		try {
			input.close();
			output.close();
			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/*
	 * manage data of each section
	 */
	private GPGGA convertNMEA(String msg) {
		GPGGA result = new GPGGA();
		String[] lstData = msg.split(",");
		String data;
		
		for (int i = 0; i < lstData.length; i++) {
			data = lstData[i];
			switch (i) {
			case 0: {
				result.setId(data);
				break;
			}
			case 1: {
				result.setTime(Double.parseDouble(data));
				break;
			}
			case 2: {
				result.setLatitude(Double.parseDouble(data));
				break;
			}
			case 3: {
				result.setDirectionLatitude(data);
				break;
			}
			case 4: {
				result.setLongitude(Double.parseDouble(data));
				break;
			}
			case 5: {
				result.setDirectionLongitude(data);
				break;
			}
			case 6: {
				result.setGpsStatus(Integer.parseInt(data));
				break;
			}
			case 7: {
				result.setNumberOfSats(Integer.parseInt(data));
				break;
			}
			default:
				break;
			}

			if (i >= 8) {
				break;
			}
		}
		return result;
	}
	
	// Getter and Setter
	// ---------------------------------------------------------------------------------------------

	public ClientUI getUi() {
		return ui;
	}

	public GPGGA getGpGGA() {
		return gpGGA;
	}

	public void setGpGGA(GPGGA gpGGA) {
		this.gpGGA = gpGGA;
	}

}