package com.nmea_object.cores;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

import com.enums.Direction;
import com.enums.Indicator;
import com.nmea_object.cores.model.GPGGA;
import com.nmea_object.gui.ServerUI;
import com.util.Utils;

public class SocketServer implements Runnable {

	private ServerSocket serverSocket = null;
	private Socket socket = null;
	private Thread thread = null;
	private ObjectInputStream input;
	private ObjectOutputStream output;
	private ServerUI ui;

	public SocketServer(ServerUI ui) {
		super();
		this.ui = ui;
	}

	public void startServerSocket(int port) {
		try {
			serverSocket = new ServerSocket(port);
			System.out.println("Server started: " + serverSocket);

			thread = new Thread(this);
			thread.start();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	/*
	 * Send message
	 */
	public void sendMessage() {
		GPGGA gpGGA = new GPGGA();
		gpGGA.setId("$GPGGA");
		gpGGA.setTime(Utils.reverseLatitude(getUi().getFieldTimeZoneS().getText()));
		gpGGA.setLatitude(Utils.reverseLatitude(getUi().getFieldLatitudeS().getText()));
		gpGGA.setDirectionLatitude(getUi().getComboDirectionLatitudeS().getSelectedItem().toString());
		gpGGA.setLongitude(Utils.reverseLongitude(getUi().getFieldLongitudeS().getText()));
		gpGGA.setDirectionLongitude(getUi().getComboDirectionLongitudeS().getSelectedItem().toString());
		gpGGA.setGpsStatus(getUi().getComboGPSS().getSelectedIndex());
		gpGGA.setNumberOfSats(Integer.parseInt(getUi().getFieldSVsS().getText()));

		try {
			output.writeObject(gpGGA);
			output.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/*
	 * Receive message
	 */
	@Override
	public void run() {
		try {
			socket = serverSocket.accept();
			output = new ObjectOutputStream(socket.getOutputStream());
			output.flush();
			input = new ObjectInputStream(socket.getInputStream());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		while (true) {
			try {
				convertNMEA((GPGGA) input.readObject());
			} catch (ClassNotFoundException e) {
				stop();
			} catch (IOException e) {
				stop();
			}
		}
	}

	public void stop() {
		try {
			input.close();
			output.close();
			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/*
	 * manage data of each section
	 */
	private void convertNMEA(GPGGA data) {
		getUi().getFieldID().setText(data.getId());
		getUi().getFieldTimeZone().setText(Utils.convertTimeZone(String.valueOf(data.getTime())));
		getUi().getFieldLatitude().setText(Utils.convertLatitude(String.valueOf(data.getLatitude()),
				Direction.findValue(data.getDirectionLatitude())));
		getUi().getFieldLongitude().setText(Utils.convertLongitude(String.valueOf(data.getLongitude()),
				String.valueOf(data.getDirectionLongitude())));
		getUi().getFieldGPS().setText(Indicator.findValue(data.getGpsStatus()));
		getUi().getFieldSVs().setText(String.valueOf(data.getNumberOfSats()));
	}

	// Getter and Setter
	// ---------------------------------------------------------------------------------------------

	public ServerUI getUi() {
		return ui;
	}
}
