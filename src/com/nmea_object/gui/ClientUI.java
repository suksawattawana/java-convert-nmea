package com.nmea_object.gui;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.nmea_object.cores.SocketClient;
import com.util.RowHandler;
import com.util.component.ButtonStyle;
import com.util.component.JFrameStyle;
import com.util.component.LabelStyle;
import com.util.component.TextAreaStyle;
import com.util.component.TextFieldStyle;

import net.miginfocom.swing.MigLayout;

public class ClientUI extends JFrameStyle {

	private static final long serialVersionUID = 1558128410336917279L;
	private JPanel contentPane;
	private JPanel panelSend;
	private JPanel panelReceive;

	// Send
	private TextFieldStyle fieldDataSend;
	private ButtonStyle btnSend;
	private LabelStyle txtSend;
	private LabelStyle txtData;

	// Receive
	private LabelStyle txtReceive;
	private TextAreaStyle areaDataReceive;

	private RowHandler rowContentPane = new RowHandler();
	private RowHandler rowPanelSend = new RowHandler();
	private RowHandler rowPanelReceive = new RowHandler();
	private SocketClient socketClient;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					new ClientUI().setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public ClientUI() {
		socketClient = new SocketClient(this);
		getSocketClient().connectServer("localhost", 1350);
		initComponent();

	}

	private void initComponent() {
		customComponent();
		actionComponent();
		manageLayout();
		setContentPane(contentPane);
	}

	private void manageLayout() {
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new MigLayout("", "[grow]", "[][grow][][grow]"));
		contentPane.setBackground(Color.gray);
		contentPane.add(txtSend, rowContentPane.manageRow(0, "alignx center"));
		contentPane.add(panelSend, rowContentPane.manageRow(0, "grow", true));
		contentPane.add(txtReceive, rowContentPane.manageRow(0, "alignx center", true));
		contentPane.add(panelReceive, rowContentPane.manageRow(0, "grow", true));

		// Send
		panelSend.setLayout(new MigLayout("", "[][grow][]", "[]"));
		panelSend.add(txtData, rowPanelSend.manageRow(0));
		panelSend.add(fieldDataSend, rowPanelSend.manageRow(1, "growx"));
		panelSend.add(btnSend, rowPanelSend.manageRow(2));

		// Receive
		panelReceive.setLayout(new MigLayout("", "[grow]", "[]"));
		panelReceive.add(areaDataReceive, rowPanelReceive.manageRow(0, "growx"));
	}

	private void actionComponent() {
		btnSend.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				getSocketClient().sendMessage(fieldDataSend.getText());
			}
		});
	}

	private void customComponent() {
		contentPane = new JPanel();
		panelSend = new JPanel();
		panelReceive = new JPanel();

		// Send
		txtSend = new LabelStyle("Send");
		txtData = new LabelStyle("Data");
		fieldDataSend = new TextFieldStyle();
		btnSend = new ButtonStyle("Send");

		// Receive
		txtReceive = new LabelStyle("Receive");
		areaDataReceive = new TextAreaStyle();
		areaDataReceive.setBounds(100, 100, 200, 200);

		fieldDataSend
				.setText("$GPGGA,172814.0,3723.46587704,N,12202.26957864,W,2,6,1.2,18.893,M,-25.669,M,2.0,0031*4F");
	}

	// Getter and Setter ------------------------------------------------

	public SocketClient getSocketClient() {
		return socketClient;
	}

	public TextAreaStyle getAreaDataReceive() {
		return areaDataReceive;
	}

}
