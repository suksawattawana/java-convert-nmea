package com.enums;

public enum Direction {
	N("N", "North", "Latitude"), S("S", "South", "Latitude"), E("E", "East", "Longitude"), W("W", "West", "Longitude");

	private String key;
	private String value;
	private String direction;

	private Direction(String key, String value, String direction) {
		this.key = key;
		this.value = value;
		this.direction = direction;
	}

	public static String findValue(String key) {
		String result = null;
		for (Direction data : Direction.values()) {
			if (data.getKey().equals(key)) {
				result = data.getValue();
			}
		}
		return result;
	}

	public static String[] findKeyByLatitude() {
		String[] result = new String[2];
		int i = 0;
		for (Direction data : Direction.values()) {
			if (data.getDirection().equals("Latitude")) {
				result[i] = data.getKey();
				i++;
			}
		}
		return result;
	}

	public static String[] findKeyByLongitude() {
		String[] result = new String[2];
		int i = 0;
		for (Direction data : Direction.values()) {
			if (data.getDirection().equals("Longitude")) {
				result[i] = data.getKey();
				i++;
			}
		}
		return result;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

}
