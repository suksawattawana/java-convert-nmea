package com.enums;

public enum Indicator {

	FIX_NOT(0,"Fix not valid"),
	GPS_FIX(1,"GPS fix"),
	DIFFEREN(2,"Differential GPS fix, OmniSTAR VBS"),
	REAL_TIME_FIXED(4,"Real-Time Kinematic, fixed integers"),
	REAL_TIME_FLOAD(5,"Real-Time Kinematic, float integers, OmniSTAR XP/HP or Location RTK");
	
	private int id;
	private String value;
	
	private Indicator(int id, String value) {
		this.id = id;
		this.value = value;
	}

	public static String findValue(int id){
		String result = null;
		for (Indicator data : Indicator.values()) {
			if (data.getId() == id) {
				result = data.getValue();
			}
		}
		return result;
	}
	
	public static String findValue(String id){
		String result = null;
		for (Indicator data : Indicator.values()) {
			if (data.getId() == Integer.parseInt(id)) {
				result = data.getValue();
			}
		}
		return result;
	}
	
	public static String[] listValue(){
		String result[] = new String[5];
		int i=0;
		for (Indicator data : Indicator.values()) {
			result[i] = data.getValue();
			i++;
		}
		return result;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
}
